Files included

OritClass : Program codes, functions

common.php : Common autoload files

wordCount.php : The base file

big.txt : The text files to be used to enter text

sql_queries : Quesries to build the database and tables

Test Assignment.pdf : The testing question

.gitignore : gitignore files

How to run
--------------------

Run the code from terminal 

$php wordCount.php big.txt 

Change the content of big.txt, in case if content need to change.