<?php

namespace Orit;

use Orit\OritClass\OritApp;

require 'common.php';


$fileName = $argv[1];

$orit = new OritApp();
$content = $orit->readTextFile($fileName);
if(!$content)   {
    var_dump('Some error occured on reading file. Process abort.');
    die;
}
$wordCount = $orit->saveDistinctWords($content);
if ($wordCount) {
    var_dump('Distinct unique words: ' . $wordCount);
} else {
    var_dump('Some error occured during insertion.');
}

$compareWords= $orit->compareWords();
var_dump($compareWords);
die;
