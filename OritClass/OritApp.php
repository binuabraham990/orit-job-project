<?php

namespace Orit\OritClass;

use Orit\OritClass\Base;

class OritApp extends Base {

    /**
     * Receive the filename and output the content if file available
     * @param type $fileName
     * @return boolean
     */
    public function readTextFile($fileName = '') {
        if ($fileName == '') {
            return FALSE;
        }
        $extension = substr($fileName, strlen($fileName) - 3, strlen($fileName));
        if ($extension != 'txt') {
            return FALSE;
        }
        $fh = fopen($fileName, 'r');
        $content = fread($fh, filesize($fileName));
        fclose($fh);
        return $content;
    }

    /**
     * Save distinct unique words to the database table distinct_words
     * @param type $content
     * @return type
     */
    public function saveDistinctWords($content) {

        $string = preg_split("/[\s,]+/", strtolower($content));
        $words = array_unique($string);
        $implodeString = implode(',', $words);
        if ($implodeString != '') {
            
            mysqli_query($this->conn, "TRUNCATE TABLE distinct_words");
            $inserted = mysqli_query($this->conn, "INSERT INTO distinct_words (words) VALUES ('$implodeString')");
        }

        if ($inserted) {
            return count($words);
        } else {
            return NULL;
        }
    }

    /**
     * Select words saved in the database
     * @param type $tableName
     * @return type
     */
    public function getWords($tableName) {

        $query = "SELECT words FROM " . $tableName . " WHERE id = 1;";
        $r = mysqli_query($this->conn, $query);
        if ($r) {
            return mysqli_fetch_row($r)[0];
        } else {
            return NULL;
        }
    }

    /**
     * Compare distinct values and watch list values and output common
     * @return type
     */
    public function compareWords() {

        $watchListWords = explode(',', $this->getWords('watch_list'));
        $distinctWords = explode(',', $this->getWords('distinct_words'));
        $commonWords = array_intersect($watchListWords, $distinctWords);
        return $commonWords;
    }

}
