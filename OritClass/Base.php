<?php

namespace Orit\OritClass;

class Base {

    private $host;
    private $username;
    private $password;
    private $db;
    protected $APPPATH;
    protected $conn;

    public function __construct() {
        $this->APPPATH = __DIR__;
        $this->host = 'localhost';
        $this->db = 'orit';
        $this->username = 'root';
        $this->password = '#';

        $this->conn = mysqli_connect($this->host, $this->username, $this->password, $this->db);

        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
    }

    public function __autoload($className) {
        require_once __DIR__ . "/OritClass/" . $className . ".php";
    }

}
